use schema::*;
use chrono;

#[derive(Insertable)]
#[table_name = "messages"]
pub struct NewMessage<'a> {
    pub content: &'a str,
    pub user_id: i32,
    pub room_id: i32,
}

#[derive(Queryable)]
#[table_name = "messages"]
pub struct Message {
    pub id: i32, 
    pub content: String,
    pub user_id: i32,
    pub room_id: i32,
    pub creation_date: chrono::NaiveDateTime,
}

#[derive(Insertable)]
#[table_name = "rooms"]
pub struct NewRoom<'a> {
    pub name: &'a str,
}

#[derive(Queryable)]
#[table_name = "rooms"] 
pub struct Room {
    pub id: i32,
    pub name: String,
}

#[derive(Queryable, PartialEq)]
#[table_name = "users"]
pub struct User {
    pub id: i32,
}
