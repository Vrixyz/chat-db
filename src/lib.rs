#![feature(custom_attribute)]

#[macro_use]
extern crate diesel;

extern crate chrono;
extern crate dotenv;
extern crate r2d2;

pub mod models;
pub mod schema;
mod connection;

use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;

use connection::*;
use diesel::RunQueryDsl;
use models::*;

pub fn establish_connection() -> r2d2::Pool<Manager<PgConnection>> {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = Manager::<PgConnection>::new(database_url);
    r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.")
}

pub fn create_user(connection: &PgConnection) -> Result<User, diesel::result::Error> {
    use schema::users;

    diesel::insert_into(users::table)
        .default_values()
        .get_result(connection)
}

pub fn get_users(connection: &PgConnection) -> Result<Vec<User>, diesel::result::Error> {
    use schema::users::dsl::*;
    users.limit(10).load::<User>(connection)
}

pub fn delete_user(
    connection: &PgConnection,
    id_to_delete: i32,
) -> Result<usize, diesel::result::Error> {
    use schema::users::dsl::*;

    diesel::delete(users.filter(id.eq(id_to_delete))).execute(connection)
}

pub fn create_room(connection: &PgConnection, name: &str) -> Result<Room, diesel::result::Error> {
    use schema::rooms;

    let new_room = NewRoom { name };

    diesel::insert_into(rooms::table)
        .values(&new_room)
        .get_result(connection)
}

pub fn delete_room(
    connection: &PgConnection,
    id_to_delete: i32,
) -> Result<usize, diesel::result::Error> {
    use schema::rooms::dsl::*;
    diesel::delete(rooms.filter(id.eq(id_to_delete))).execute(connection)
}

pub fn get_rooms(connection: &PgConnection) -> Result<Vec<Room>, diesel::result::Error> {
    use schema::rooms::dsl::*;
    rooms.limit(10).load::<Room>(connection)
}

pub fn create_message(
    connection: &PgConnection,
    user_id: i32,
    room_id: i32,
    content: &str,
) -> Result<Message, diesel::result::Error> {
    use schema::messages;

    let new_message = NewMessage {
        user_id,
        room_id,
        content,
    };

    diesel::insert_into(messages::table)
        .values(&new_message)
        .get_result(connection)
}

pub fn get_messages(connection: &PgConnection) -> Result<Vec<Message>, diesel::result::Error> {
    use schema::messages::dsl::*;
    messages.limit(10).load::<Message>(connection)
}

pub fn delete_message(
    connection: &PgConnection,
    id_to_delete: i32,
) -> Result<usize, diesel::result::Error> {
    use schema::messages::dsl::*;
    diesel::delete(messages.filter(id.eq(id_to_delete))).execute(connection)
}

#[cfg(test)]
mod tests {
    use std::panic;
    use *;

    #[test]
    fn test_users() {
        run_test(
            || {
                let connection = get_database_connection();
                let created_user = create_user(&connection).expect("Failed to create user.");
                assert!(0 < get_users(&connection).expect("Failed to get users.").len());
                created_user
            },
            |user: &Option<User>| {
                let connection = get_database_connection();
                if let Some(user) = user {
                    assert_eq!(
                        1,
                        delete_user(&connection, user.id).expect("Failed to delete created user")
                    );
                }
            },
        )
    }

    #[test]
    fn test_rooms() {
        run_test(
            || {
                let connection = get_database_connection();
                let created_room =
                    create_room(&connection, "super room").expect("Failed to create room.");
                assert!(0 < get_rooms(&connection).expect("Failed to get rooms.").len());
                created_room
            },
            |room: &Option<Room>| {
                let connection = get_database_connection();
                if let Some(room) = room {
                    assert_eq!(
                        1,
                        delete_room(&connection, room.id).expect("Failed to delete created room")
                    );
                }
            },
        )
    }

    #[test]
    fn test_messages() {
        pub struct Data {
            room: Room,
            user: User,
            message: Message,
        };
        run_test(
            || {
                let connection = get_database_connection();
                let room = create_room(&connection, "super room").expect("Failed to create room.");
                let user = create_user(&connection).expect("Failed to create user.");
                let data = Data {
                    message: create_message(&connection, user.id, room.id, "Hello, World!")
                        .expect("Failed to create message."),
                    room,
                    user,
                };
                assert!(0 < get_rooms(&connection).expect("Failed to get rooms.").len());
                assert!(0 < get_users(&connection).expect("Failed to get users.").len());
                assert!(
                    0 < get_messages(&connection)
                        .expect("Failed to get messages.")
                        .len()
                );
                data
            },
            |data: &Option<Data>| {
                let connection = get_database_connection();
                if let Some(data) = data {
                    assert_eq!(
                        1,
                        delete_message(&connection, data.message.id)
                            .expect("Failed to delete created message")
                    );
                    assert_eq!(
                        1,
                        delete_user(&connection, data.user.id)
                            .expect("Failed to delete created user")
                    );
                    assert_eq!(
                        1,
                        delete_room(&connection, data.room.id)
                            .expect("Failed to delete created room")
                    );
                }
            },
        )
    }

    fn get_database_connection() -> r2d2::PooledConnection<Manager<PgConnection>> {
        establish_connection()
            .get()
            .expect("Failed to get connection from pool.")
    }

    fn run_test<T, F, D>(test: T, teardown: F) -> ()
    where
        T: FnOnce() -> D + panic::UnwindSafe,
        F: FnOnce(&Option<D>) -> () + panic::UnwindSafe,
        D: std::panic::RefUnwindSafe,
    {
        let test_result = panic::catch_unwind(|| test());
        let data = match test_result {
            Ok(data) => Some(data),
            Err(_) => None,
        };
        let teardown_result = panic::catch_unwind(|| {
            teardown(&data);
        });

        assert!(data.is_some());
        assert!(teardown_result.is_ok())
    }
}
