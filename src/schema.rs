table! {
    messages (id) {
        id -> Int4,
        content -> Varchar,
        user_id -> Int4,
        room_id -> Int4,
        creation_date -> Timestamp,
    }
}

table! {
    rooms (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    users (id) {
        id -> Int4,
    }
}

joinable!(messages -> rooms (room_id));
joinable!(messages -> users (user_id));

allow_tables_to_appear_in_same_query!(messages, rooms, users,);
