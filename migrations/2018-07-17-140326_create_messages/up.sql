-- Your SQL goes here

CREATE TABLE messages (
  id SERIAL PRIMARY KEY,
  content VARCHAR NOT NULL,
  user_id integer NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  room_id integer NOT NULL REFERENCES rooms(id) ON DELETE CASCADE,
  creation_date TIMESTAMP NOT NULL DEFAULT NOW()
)